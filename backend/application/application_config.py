from db_handle.enums import TaskKind


DAEMON_NAME: str = "mahds.service"

MAX_USER_TASKS: int = 5
MAX_ADMIN_TASKS: int = 100
CLUSTER_MANAGER_NODE: str = "masternode"


SEQ_TYPE_RDNA: str = 'rdna'
SEQ_TYPE_PROTEIN: str = 'protein'
SEQ_TYPE_PROTEIN_REDUCED: str = 'proteinReduced'
AL_TYPE_GLOBAL: str = 'global'
AL_TYPE_LOCAL: str = 'local'
AL_TYPE_BEST_INTERSECTION: str = 'mixed'


PROCESSING_SLEEP_TIME: int = 3
PROCESSING_LOOP_SCRIPT: str = "mahds_process_tasks.py"
PROCESSING_LOOP_SCRIPT_LOG: str = "mahds_process_tasks.log"
PROCESSING_DIR: str = "/pool_hot/mahds_site/backend/mahds/"
# PROCESSING_LAUNCHER: str = "mahds_launcher.sh"
PROCESSING_ALIGN_APP: str = "mahds_align"
PROCESSING_ASSESS_APP: str = "mahds_assess"
PROCESSING_CONFIG_DIR: str = "config.d/"
PROCESSING_CONFIG_METHOD_FILE: str = "mahds_method.yaml"
PROCESSING_CONFIG_SMAPPINGS_DIR: str = "mappings/"
PROCESSING_CONFIG_SMAPPING_FILE: str = "mahds_sym_mapping.yaml"
PROCESSING_SEQ_TYPE_SMAPPING_FILES: dict[str, str] = {
    SEQ_TYPE_RDNA: "rdna_standard_mapping.yaml",
    SEQ_TYPE_PROTEIN: "protein_standard_mapping.yaml",
    SEQ_TYPE_PROTEIN_REDUCED: "protein_5classes_mapping.yaml"
}

PROCESSING_RESULT_STORE_TIME: int = 7 # days


# SLURM_WAIT_SLEEP_TIME: int = 5
SLURM_PARTITION: str = "ryzen"
SLURM_AVAILABLE_NODES: int = 2
SLURM_NTASKS_PER_NODE: int = 1
SLURM_NTASKS_PER_SOCKET: int = 1
SLURM_CPUS_PER_TASK: int = 16
SLURM_TIME: str = "240:00:00"


REQUIRED_NODES_MAP: dict[TaskKind, int] = {
    TaskKind.ALIGNMENT: 2,
    TaskKind.ASSESSMENT: 1
}
