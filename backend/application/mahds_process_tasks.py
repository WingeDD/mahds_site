#!/bin/python3


from typing import Optional
import os
import sys
import shutil
import time
import subprocess
import datetime
import re
import json
from sqlalchemy import create_engine, select, delete, update, func, or_
from sqlalchemy.orm import sessionmaker

SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))
sys.path.append(os.path.dirname(SCRIPT_DIR))

from db_handle.db_tables import Tasks, Results
from db_handle.enums import TaskField, ResultField, TaskStatus, TaskKind, ResultStatus
from db_handle.utils import row2dict
import application.application_config as cfg
from config import SQLALCHEMY_DATABASE_URI


LOG_PATH: str = os.path.join(SCRIPT_DIR, cfg.PROCESSING_LOOP_SCRIPT_LOG)
CONFIG_DIR: str = os.path.join(cfg.PROCESSING_DIR, cfg.PROCESSING_CONFIG_DIR)
CONFIG_METHOD_FILE: str = os.path.join(CONFIG_DIR, cfg.PROCESSING_CONFIG_METHOD_FILE)
CONFIG_SMAPPING_FILE: str = os.path.join(CONFIG_DIR, cfg.PROCESSING_CONFIG_SMAPPING_FILE)
CONFIG_SMAPPINGS_DIR: str = os.path.join(CONFIG_DIR, cfg.PROCESSING_CONFIG_SMAPPINGS_DIR)



re_sbatch_access_slurm_resp = re.compile(r'^\s*Submitted\s*batch\s*job\s*(\d+)\s*$', flags=re.RegexFlag.IGNORECASE)
re_squeue_failure_slurm_resp = re.compile(r'.*invalid\s*job\s*id.*', flags=re.RegexFlag.IGNORECASE)
re_bad_slurm_out = re.compile(r'(?:terminated|error|fail)', flags=re.RegexFlag.IGNORECASE)


GET_INPUT_PATH = lambda task_id: os.path.join(cfg.PROCESSING_DIR, f"input_{task_id}.fasta")
GET_OUTPUT_PATH = lambda task_id: os.path.join(cfg.PROCESSING_DIR, f"output_{task_id}.yaml")
GET_LAUNCHER_PATH = lambda task_id: os.path.join(cfg.PROCESSING_DIR, f"launcher_{task_id}.sh")


def get_and_set_task_executing(session, logfile, busy_nodes: int) -> Optional[dict]:
    min_date: Optional[int] = session.execute( select(func.min(Tasks.date)).where(getattr(Tasks, TaskField.STATUS) == TaskStatus.QUEUED.value) ).scalar_one() # date or None
    if min_date is None:
        return None
    row = session.execute(select(Tasks).where(getattr(Tasks, TaskField.DATE) == min_date)).one()
    if (busy_nodes + cfg.REQUIRED_NODES_MAP[getattr(row.Tasks, TaskField.KIND)]) > cfg.SLURM_AVAILABLE_NODES:
        return None
    res = {
        TaskField.ID: getattr(row.Tasks, TaskField.ID),
        TaskField.DATE: getattr(row.Tasks, TaskField.DATE),
        TaskField.OWNER: getattr(row.Tasks, TaskField.OWNER),
        TaskField.NAME: getattr(row.Tasks, TaskField.NAME),
        TaskField.KIND: getattr(row.Tasks, TaskField.KIND)
    }
    try:
        session.execute( update(Tasks).where(getattr(Tasks, TaskField.ID) == res[TaskField.ID]).values( **{TaskField.STATUS.value: TaskStatus.EXECUTING.value} ) )
        with open(GET_INPUT_PATH(res[TaskField.ID]) , 'w') as file:
            file.write(getattr(row.Tasks, TaskField.DATA))
        params_serialized: str = getattr(row.Tasks, TaskField.PARAMETERS) # json str
        params: dict = json.loads(params_serialized)
        smapping_fname: str = cfg.PROCESSING_SEQ_TYPE_SMAPPING_FILES[params["sequencesType"]]
        shutil.copyfile(os.path.join(CONFIG_SMAPPINGS_DIR, smapping_fname), CONFIG_SMAPPING_FILE)
        del params["sequencesType"]
        with open(CONFIG_METHOD_FILE, "w") as file:
            json.dump(params, file)
    except Exception as e:
        logfile.write(f"In get_and_set_task_executing: {repr(e)}\n")
        create_result_err_set_task_done(session, res)
        raise e
    return res


def set_exec_id(session, task_id: int, exec_id: int):
    session.execute( update(Tasks).where(getattr(Tasks, TaskField.ID) == task_id).values( **{TaskField.EXECID.value: exec_id} ) )


def create_result(session, task_properties: set, status: ResultStatus, data: Optional[str]) -> None:
    res: Results = Results()
    setattr(res, ResultField.ID, task_properties[TaskField.ID])
    setattr(res, ResultField.OWNER, task_properties[TaskField.OWNER])
    setattr(res, ResultField.NAME, task_properties[TaskField.NAME])
    setattr(res, ResultField.STATUS, status.value)
    setattr(res, ResultField.DATA, data)
    session.add(res)


def create_result_ok_set_task_done(session, task_properties: set) -> None:
    id: int = task_properties[TaskField.ID]
    session.execute( update(Tasks).where(getattr(Tasks, TaskField.ID) == id).values( **{TaskField.STATUS.value: TaskStatus.DONE.value} ) )
    res_data: Optional[str] = None
    with open(GET_OUTPUT_PATH(task_properties[TaskField.ID]), 'r') as file:
        res_data = file.read()
    create_result(session, task_properties, ResultStatus.OK, res_data)


def create_result_err_set_task_done(session, task_properties: set) -> None:
    id: int = task_properties[TaskField.ID]
    create_result(session, task_properties, ResultStatus.ERROR, None)
    session.execute( update(Tasks).where(getattr(Tasks, TaskField.ID) == id).values( **{TaskField.STATUS.value: TaskStatus.DONE.value} ) )


def set_result_err(session, id: int):
    session.execute( update(Results).where(getattr(Results, ResultField.ID) == id).values( **{ResultField.STATUS.value: ResultStatus.ERROR.value} ) )

# "slurm-%j.out"
def make_launcher_slurm(task_id: int, kind: TaskKind) -> None:
    with open(GET_LAUNCHER_PATH(task_id), 'w') as file:
        file.write(f"#!/bin/bash\n")
        file.write(f"#\n")
        file.write(f"#SBATCH --partition={cfg.SLURM_PARTITION}\n")
        file.write(f"#SBATCH --chdir={cfg.PROCESSING_DIR}\n")
        file.write(f"#SBATCH --output={os.path.join(cfg.PROCESSING_DIR, 'slurm-%j.out')}\n")
        file.write(f"#SBATCH --error={os.path.join(cfg.PROCESSING_DIR, 'slurm-%j.out')}\n")
        file.write(f"#SBATCH --nodes={cfg.REQUIRED_NODES_MAP[kind]}\n")
        file.write(f"#SBATCH --ntasks-per-node={cfg.SLURM_NTASKS_PER_NODE}\n")
        file.write(f"#SBATCH --ntasks-per-socket={cfg.SLURM_NTASKS_PER_SOCKET}\n")
        file.write(f"#SBATCH --cpus-per-task={cfg.SLURM_CPUS_PER_TASK}\n")
        file.write(f"#SBATCH --time={cfg.SLURM_TIME}\n\n")
        file.write(f"cd {cfg.PROCESSING_DIR}\n")
        if kind == TaskKind.ALIGNMENT:
            file.write(f"srun ./{cfg.PROCESSING_ALIGN_APP} {GET_INPUT_PATH(task_id)} {GET_OUTPUT_PATH(task_id)} -c {CONFIG_DIR}\n")
        elif kind == TaskKind.ASSESSMENT:
            file.write(f"srun ./{cfg.PROCESSING_ASSESS_APP} {GET_INPUT_PATH(task_id)} -o {GET_OUTPUT_PATH(task_id)} -c {CONFIG_DIR}")
        else:
            raise Exception(f"In make_launcher_slurm: unexpected task kind ({kind})")


def launch_slurm(task_id: int) -> int:
    proc = subprocess.Popen(['ssh', cfg.CLUSTER_MANAGER_NODE, "sbatch", GET_LAUNCHER_PATH(task_id)], cwd=SCRIPT_DIR, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    rcode: int = proc.wait()
    if rcode != 0:
        raise Exception(f"sbatch return code: {rcode}, stdout: {proc.stdout.read().decode('utf-8')}, stderr: {proc.stderr.read().decode('utf-8')}")
    out: str = (proc.stdout.read()).decode("utf-8")
    parsed = re_sbatch_access_slurm_resp.search(out)
    if parsed is None:
        raise Exception(f"sbatch stdout: {out}, stderr: {(proc.stderr.read()).decode('utf-8')}")
    return int(parsed[1])


def check_task_done(slurm_task_id: int) -> bool:
    proc = subprocess.Popen(['ssh', cfg.CLUSTER_MANAGER_NODE, "squeue", "-j", str(slurm_task_id)], cwd=SCRIPT_DIR, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    rcode: int = proc.wait()
    out: str = (proc.stdout.read()).decode("utf-8") 
    err: str = (proc.stderr.read()).decode("utf-8") 
    if rcode != 0 and re_squeue_failure_slurm_resp.match(err):
        return True
    elif rcode != 0:
        raise Exception(f"squeue return code: {rcode}, stdout: {out}, stderr: {err}")
    # return code == 0
    lines: int = 0
    for sym in out:
        if sym == '\n':
            lines += 1
    # header + [job record]
    if lines < 2:
        return True
    return False


def cancel_slurm(slurm_task_id: int) -> None:
    "BLOCKING"
    while(True):
        proc = subprocess.Popen(['ssh', cfg.CLUSTER_MANAGER_NODE, "scancel", str(slurm_task_id)], cwd=SCRIPT_DIR, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        rcode: int = proc.wait()
        if rcode != 0:
            raise Exception(f"scancel return code: {rcode}, stdout: {proc.stdout.read().decode('utf-8')}, stderr: {proc.stderr.read().decode('utf-8')}")
        out: str = (proc.stdout.read()).decode("utf-8")
        if out != "":
            raise Exception(f"scancel stdout: {out}, stderr: {(proc.stderr.read()).decode('utf-8')}")
        done: bool = check_task_done(slurm_task_id)
        if done:
            break


def clear(slurm_task_id: int, task_id: int, check_slurm_out: bool = True) -> None:
    slurm_out_file_path: str = os.path.join(cfg.PROCESSING_DIR, f"slurm-{slurm_task_id}.out")
    if check_slurm_out:
        with open(slurm_out_file_path, 'r') as file:
            if (re_bad_slurm_out.search(file.read())):
                raise Exception(f"Error: found error in {slurm_out_file_path}; slurm_task_id: {slurm_task_id}; task_id: {task_id}")
    lp: str = GET_LAUNCHER_PATH(task_id)
    ip: str = GET_INPUT_PATH(task_id)
    op: str = GET_OUTPUT_PATH(task_id)
    if os.path.exists(slurm_out_file_path):
        os.remove(slurm_out_file_path)
    if os.path.exists(lp):
        os.remove(lp)
    if os.path.exists(ip):
        os.remove(ip)
    if os.path.exists(op):
        os.remove(op)


def delete_old(session, logfile) -> None:
    threshold_date = datetime.datetime.utcnow() - datetime.timedelta(days=cfg.PROCESSING_RESULT_STORE_TIME)
    old_results = session.execute(select(Results.id).where(getattr(Results, ResultField.DATE) < threshold_date)).all()
    for old_result in old_results:
        id: int = old_result[0]
        session.execute( delete(Results).where(getattr(Results, ResultField.ID) == id) )
        session.execute( delete(Tasks).where(getattr(Tasks, TaskField.ID) == id) )
        logfile.write(f"Result {id} deleted\n")
        session.commit()



def launch() -> None:
    while True:
        time.sleep(cfg.PROCESSING_SLEEP_TIME)

        # set logging
        try:
            log = open(LOG_PATH, "a")
        except Exception as e:
            print(repr(e))
            continue
        
        # set db
        try:
            engine = create_engine(SQLALCHEMY_DATABASE_URI)
            Sessionmaker = sessionmaker(engine)
        except Exception as e:
            log.write(f"{repr(e)}\n")
            log.close()
            continue

        # delete old
        try:
            session = Sessionmaker()
        except Exception as e:
            log.write(f"{repr(e)}\n")
            log.close()
            continue

        try:
            delete_old(session, log) # commits itself
        except Exception as e:
            log.write(f"{repr(e)}\n")
        finally:
            session.close()


        # checking current tasks
        try:
            session = Sessionmaker()
        except Exception as e:
            log.write(f"{repr(e)}\n")
            log.close()
            continue
        try:
            cur_tasks = session.execute(select(
                Tasks.id, Tasks.execid, Tasks.status, Tasks.owner, Tasks.name, Tasks.kind
            ).where(or_(
                getattr(Tasks, TaskField.STATUS) == TaskStatus.EXECUTING.value,
                getattr(Tasks, TaskField.STATUS) == TaskStatus.DELETING.value
            ))).all()
        except Exception as e:
            log.write(f"{repr(e)}\n")
            log.close()
            session.close()
            continue
        busy_nodes: int = 0
        try:
            for cur_task in cur_tasks:
                cur_task = cur_task._asdict()
                if (cur_task[TaskField.STATUS] == TaskStatus.EXECUTING):
                    try:
                        done: bool = check_task_done(cur_task[TaskField.EXECID])
                    except Exception as e:
                        log.write(f"{repr(e)}\n")
                        continue
                    if done:
                        try:
                            create_result_ok_set_task_done(session, cur_task)
                        except Exception as e:
                            log.write(f"{repr(e)}\n")
                            create_result_err_set_task_done(session, cur_task)
                        try:
                            clear(cur_task[TaskField.EXECID], cur_task[TaskField.ID], True)
                            log.write(f"Task completed: {cur_task}\n")
                        except Exception as e:
                            log.write(f"{repr(e)}\n")
                            set_result_err(session, cur_task[TaskField.ID])
                        finally:
                            session.commit()
                    else:
                        try:
                            busy_nodes += cfg.REQUIRED_NODES_MAP[cur_task[TaskField.KIND]]
                        except Exception as e:
                            log.write(f"{repr(e)}\n")
                            continue
                elif cur_task[TaskField.STATUS] == TaskStatus.DELETING:
                    try:
                        session.execute( delete(Results).where(getattr(Results, ResultField.ID) == cur_task[TaskField.ID]) )
                        session.execute( delete(Tasks).where(getattr(Tasks, TaskField.ID) == cur_task[TaskField.ID]) )
                        cancel_slurm(cur_task[TaskField.EXECID])
                        clear(cur_task[TaskField.EXECID], cur_task[TaskField.ID], False)
                        session.commit()
                    except Exception as e:
                        log.write(f"{repr(e)}\n")
                        session.rollback()
                else:
                    log.write(f"Unexpected task status while checking current tasks: {cur_task[TaskField.STATUS]}\n")
                    continue
            session.close()
        except Exception as e:
            log.write(f"{repr(e)}\n")
            log.close()
            session.close()
            continue


        # getting task info + setting setup files + sending to cluster
        try:
            session = Sessionmaker()
        except Exception as e:
            log.write(f"{repr(e)}\n")
            log.close()
            continue
        try:
            task_properties: Optional[dict] = get_and_set_task_executing(session, log, busy_nodes)
            if task_properties is None:
                log.close()
                session.close()
                continue
            make_launcher_slurm(task_properties[TaskField.ID], task_properties[TaskField.KIND])
            slurm_task_id: int = launch_slurm(task_properties[TaskField.ID])
            set_exec_id(session, task_properties[TaskField.ID], slurm_task_id)
        except Exception as e:
            create_result_err_set_task_done(session, task_properties)
            log.write(f"{repr(e)}\n")
        finally:
            session.commit()
            session.close()
            log.close()



if __name__ == "__main__":
    while True:
        try:
            launch()
        except:
            pass
