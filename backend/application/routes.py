from typing import Optional
from flask import current_app, render_template, request, json
from flask_login import login_required, current_user
from flask_api import status
from sqlalchemy.exc import NoResultFound
from auth.decorators import is_verified, status_required
from application import application
from db_handle.enums import UserStatus, TaskField, ResultField, TaskKind, TaskStatus
from db_handle.db_tables import Tasks, Results
from db_handle.init import db
from db_handle.utils import row2dict
from application.tasks_processing import check_tasks_processor, validate_config, validate_fasta
import application.application_config as cfg


@application.route("", methods=['GET'])
@application.route("/", methods=['GET'])
@application.route("/main", methods=['GET'])
@application.route("/index", methods=['GET'])
def main():
    try:
        return current_app.response_class(
            response=render_template('index.html'),
            status=status.HTTP_200_OK,
            mimetype='text/html'
        )
    except Exception as e:
        return current_app.response_class(
            response=json.dumps(repr(e)),
            status=status.HTTP_500_INTERNAL_SERVER_ERROR,
            mimetype='application/json'
        )


@application.route("/about", methods=['GET'])
def about():
    try:
        return current_app.response_class(
            response=render_template('index.html'),
            status=status.HTTP_200_OK,
            mimetype='text/html'
        )
    except Exception as e:
        return current_app.response_class(
            response=json.dumps(repr(e)),
            status=status.HTTP_500_INTERNAL_SERVER_ERROR,
            mimetype='application/json'
        )


@application.route("/result/page/<int:id>", methods=['GET'])
def result_page(id):
    try:
        return current_app.response_class(
            response=render_template('index.html'),
            status=status.HTTP_200_OK,
            mimetype='text/html'
        )
    except Exception as e:
        return current_app.response_class(
            response=json.dumps(repr(e)),
            status=status.HTTP_500_INTERNAL_SERVER_ERROR,
            mimetype='application/json'
        )


@application.route("/result/list", methods=['GET'])
@login_required
@is_verified
@status_required([UserStatus.USER, UserStatus.ADMIN])
def result_list():
    try:
        user_id: int = int(current_user.get_id())
        rows = db.session.execute(db.select(Results.id, Results.name, Results.date, Results.status).where(getattr(Results, ResultField.OWNER) == user_id)).all()
        results: list[dict] = list(map(lambda row: row._asdict(), rows))
        return current_app.response_class(
            response=json.dumps(results),
            status=status.HTTP_200_OK,
            mimetype='application/json'
        )
    except Exception as e:
        return current_app.response_class(
            response=json.dumps(repr(e)),
            status=status.HTTP_500_INTERNAL_SERVER_ERROR,
            mimetype='application/json'
        )


@application.route("/result/fetch", methods=['POST'])
@login_required
@is_verified
@status_required([UserStatus.USER, UserStatus.ADMIN])
def result_fetch():
    try:
        request_data: map = request.get_json(force=True) # id
        result_id: int = request_data['id']
        try:
            result = db.session.execute(db.select(Results).where(getattr(Results, ResultField.ID) == result_id)).one()
        except NoResultFound:
            return current_app.response_class(
                response=json.dumps({}),
                status=status.HTTP_410_GONE,
                mimetype='application/json'
            )
        user_id: int = int(current_user.get_id())
        user_status: UserStatus = current_user.get_status()
        if (getattr(result.Results, ResultField.OWNER) != user_id) and (user_status != UserStatus.ADMIN):
            return current_app.response_class(
                response=json.dumps({"reason": "ownership"}),
                status=status.HTTP_403_FORBIDDEN,
                mimetype='application/json'
            )
        res = row2dict(result.Results)
        del res[ResultField.OWNER]
        return current_app.response_class(
            response=json.dumps(res),
            status=status.HTTP_200_OK,
            mimetype='application/json'
        )
    except Exception as e:
        return current_app.response_class(
            response=json.dumps(repr(e)),
            status=status.HTTP_500_INTERNAL_SERVER_ERROR,
            mimetype='application/json'
        )


@application.route("/task/list", methods=['GET'])
@login_required
@is_verified
@status_required([UserStatus.USER, UserStatus.ADMIN])
def task_list():
    try:
        user_id: int = int(current_user.get_id())
        rows = db.session.execute(db.select(Tasks.id, Tasks.name, Tasks.date, Tasks.status, Tasks.kind).where(getattr(Tasks, TaskField.OWNER) == user_id)).all()
        results: list[dict] = list(map(lambda row: row._asdict(), rows))
        return current_app.response_class(
            response=json.dumps(results),
            status=status.HTTP_200_OK,
            mimetype='application/json'
        )
    except Exception as e:
        return current_app.response_class(
            response=json.dumps(repr(e)),
            status=status.HTTP_500_INTERNAL_SERVER_ERROR,
            mimetype='application/json'
        )
    

@application.route("/task/fetch", methods=['POST'])
@login_required
@is_verified
@status_required([UserStatus.USER, UserStatus.ADMIN])
def task_fetch():
    try:
        request_data: map = request.get_json(force=True) # id, [needParameters], [needData]
        task_id: int = request_data['id']
        required_fields: list = [Tasks.id, Tasks.owner, Tasks.name, Tasks.date, Tasks.status, Tasks.kind]
        if ('needParameters' not in request_data) or (request_data['needParameters'] is None) or request_data['needParameters']:
            required_fields.append(Tasks.parameters)
        if ('needData' not in request_data) or (request_data['needData'] is None) or request_data['needData']:
            required_fields.append(Tasks.data)
        try:
            task = db.session.execute(db.select(*required_fields).where(getattr(Tasks, TaskField.ID) == task_id)).one()
        except NoResultFound:
            return current_app.response_class(
                response=json.dumps({}),
                status=status.HTTP_410_GONE,
                mimetype='application/json'
            )
        user_id: int = int(current_user.get_id())
        user_status: UserStatus = current_user.get_status()
        if (getattr(task, TaskField.OWNER) != user_id) and (user_status != UserStatus.ADMIN):
            return current_app.response_class(
                response=json.dumps({"reason": "ownership"}),
                status=status.HTTP_403_FORBIDDEN,
                mimetype='application/json'
            )
        res = task._asdict()
        del res[TaskField.OWNER]
        return current_app.response_class(
            response=json.dumps(res),
            status=status.HTTP_200_OK,
            mimetype='application/json'
        )
    except Exception as e:
        return current_app.response_class(
            response=json.dumps(repr(e)),
            status=status.HTTP_500_INTERNAL_SERVER_ERROR,
            mimetype='application/json'
        )


@application.route("/task", methods=['PUT'])
@login_required
@is_verified
@status_required([UserStatus.USER, UserStatus.ADMIN])
def task_put():
    try:
        request_data: map = request.get_json(force=True) # [name], parameters, data
        user_id: int = int(current_user.get_id())
        user_status: UserStatus = current_user.get_status()
        tasks_count: int = db.session.execute( db.select(db.func.count()).select_from(Tasks).where(getattr(Tasks, TaskField.OWNER) == user_id) ).scalar_one()
        results_count: int = db.session.execute( db.select(db.func.count()).select_from(Results).where(getattr(Results, ResultField.OWNER) == user_id) ).scalar_one()
        task_limit: int = cfg.MAX_ADMIN_TASKS if user_status == UserStatus.ADMIN else cfg.MAX_USER_TASKS
        if (tasks_count-results_count) >= task_limit:
            return current_app.response_class(
                response=json.dumps({"reason": "count", "count": tasks_count, "limit": task_limit}),
                status=status.HTTP_403_FORBIDDEN,
                mimetype='application/json'
            )
        issue: Optional[str] = validate_config(request_data['parameters'])
        if issue is not None:
            return current_app.response_class(
                response=json.dumps({"reason": "parameters", "issue": issue}),
                status=status.HTTP_400_BAD_REQUEST,
                mimetype='application/json'
            )
        issue = validate_fasta(request_data['data'], request_data['parameters']['sequencesType'], TaskKind(request_data['kind']))
        if issue is not None:
            return current_app.response_class(
                response=json.dumps({"reason": "sequences", "issue": issue}),
                status=status.HTTP_400_BAD_REQUEST,
                mimetype='application/json'
            )
        ok = check_tasks_processor()
        if not ok:
            return current_app.response_class(
                response=json.dumps({"reason": "tasks processor is turned off (please contact us to fix this)"}),
                status=status.HTTP_500_INTERNAL_SERVER_ERROR,
                mimetype='application/json'
            )
        
        task: Tasks = Tasks()
        setattr(task, TaskField.OWNER, user_id)
        setattr(task, TaskField.NAME, request_data['name'] if 'name' in request_data else None)
        setattr(task, TaskField.KIND, TaskKind(request_data['kind']))
        setattr(task, TaskField.PARAMETERS, json.dumps(request_data['parameters']))
        setattr(task, TaskField.DATA, request_data['data'])
        db.session.add(task)
        db.session.flush()
        task_id: int = getattr(task, TaskField.ID)
        db.session.commit()
        return current_app.response_class(
            response=json.dumps({"id": task_id, "store_time": cfg.PROCESSING_RESULT_STORE_TIME}),
            status=status.HTTP_200_OK,
            mimetype='application/json'
        )
    except Exception as e:
        db.session.rollback()
        return current_app.response_class(
            response=json.dumps(repr(e)),
            status=status.HTTP_500_INTERNAL_SERVER_ERROR,
            mimetype='application/json'
        )


@application.route("/task", methods=['DELETE'])
@login_required
@is_verified
@status_required([UserStatus.USER, UserStatus.ADMIN])
def task_delete():
    try:
        request_data: map = request.get_json(force=True) # id
        task_id: int = request_data['id']
        try:
            task = db.session.execute(db.select(Tasks.id, Tasks.owner).where(getattr(Tasks, TaskField.ID) == task_id)).one()
        except NoResultFound:
            db.session.rollback()
            return current_app.response_class(
                response=json.dumps({}),
                status=status.HTTP_410_GONE,
                mimetype='application/json'
            )
        user_id: int = int(current_user.get_id())
        user_status: UserStatus = current_user.get_status()
        if (getattr(task, TaskField.OWNER) != user_id) and (user_status != UserStatus.ADMIN):
            db.session.rollback()
            return current_app.response_class(
                response=json.dumps({"reason": "ownership"}),
                status=status.HTTP_403_FORBIDDEN,
                mimetype='application/json'
            )
        db.session.execute(db.update(Tasks).where(getattr(Tasks, TaskField.ID) == task_id).values(**{TaskField.STATUS.value: TaskStatus.DELETING.value}))
        db.session.commit()
        return current_app.response_class(
            response=json.dumps({"id": task_id}),
            status=status.HTTP_200_OK,
            mimetype='application/json'
        )
    except Exception as e:
        db.session.rollback()
        return current_app.response_class(
            response=json.dumps(repr(e)),
            status=status.HTTP_500_INTERNAL_SERVER_ERROR,
            mimetype='application/json'
        )
