from typing import Optional, Iterable, Any
import os
import re
import psutil
import subprocess
from math import sqrt
from io import StringIO
from Bio.SeqIO.FastaIO import SimpleFastaParser
from enum import Enum
import application.application_config as cfg
from db_handle.enums import TaskKind


def validate_config(config: dict) -> Optional[str]:
    "most of the work is already done by api validator"
    k_d: float = -1.0
    r_mult: float = 5.
    if 'multiplierR' in config:
        r_mult = config['multiplierR']
    if 'constKd' in config:
        k_d = config['constKd']
    if abs(k_d) >= sqrt(r_mult):
        return "constKd absolute value must be less than square root of multiplierR"
    # if ('artificialLengthsRange' in config) and (type(config['artificialLengthsRange']) == dict):
    # 1 <= length of artificialLengthsRange <= 100
    return None


def chars_ci_set(chars: Iterable[str]) -> set[str]:
    return set(map(lambda c: c.lower(), chars)).union(set(map(lambda c: c.upper(), chars)))
FASTA_RDNA_CHARS: set[str] = chars_ci_set(
    ('A', 'B', 'C', 'D', 'G', 'H', 'K', 'M', 'N', 'R', 'S', 'T', 'U', 'W', 'Y')
)
FASTA_PROTEIN_CHARS: set[str] = chars_ci_set(
    ('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z')
)
ARTIFICIAL_CHARS: set[str] = set((
    '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'
))

SEQ_TYPE_CHARS: dict[str, set[str]] = {
    cfg.SEQ_TYPE_RDNA: FASTA_RDNA_CHARS,
    cfg.SEQ_TYPE_PROTEIN: FASTA_PROTEIN_CHARS,
    cfg.SEQ_TYPE_PROTEIN_REDUCED: FASTA_PROTEIN_CHARS
}

re_artif_seq_name = re.compile(r'.*artificial\s*pivot\s*sequence.*', flags=re.RegexFlag.IGNORECASE)


def validate_fasta(data: str, seq_type: str, kind: TaskKind) -> Optional[str]:
    "returns an issue message or None"
    permitted_chars: set[str] = SEQ_TYPE_CHARS[seq_type]
    parser = SimpleFastaParser(StringIO(data))
    names: set[str] = set()
    parsed: bool = False
    if kind == TaskKind.ALIGNMENT:
        for name, seq in parser:
            parsed = True
            if name in names:
                return f"FASTA header duplicate: {name}"
            for char in seq:
                if (char not in permitted_chars) and (not char.isspace()):
                    return f"Unexpected character '{char}' in {name}"
            names.add(name)
    elif kind == TaskKind.ASSESSMENT:
        ma_len: Optional[int] = None
        for name, seq in parser:
            parsed = True
            if name in names:
                return f"FASTA header duplicate: {name}"
            if re_artif_seq_name.match(name):
                for char in seq:
                    if (char not in ARTIFICIAL_CHARS) and (not char.isspace()) and (char != '-'):
                        return f"Unexpected character '{char}' in {name}"
            else:
                seq_len: int = 0
                for char in seq:
                    if (char in permitted_chars) or (char == '-'):
                        seq_len += 1
                    elif not char.isspace():
                        return f"Unexpected character '{char}' in {name}"
                if ma_len is None:
                    ma_len = seq_len
                else:
                    if ma_len != seq_len:
                        return f"Aligned sequence {name} has {seq_len} non-space characters while previous aligned sequences had {ma_len} characters"
                names.add(name)
    else:
        return f"Unexpected task kind: {kind}"

    if not parsed:
        return "Given data can not be parsed as FASTA"
    if len(names) < 2:
        return f"Only {len(names)} sequences were given"
    return None


def task_processor_start() -> bool:
    path: str = os.path.realpath(os.path.dirname(__file__))
    proc = subprocess.Popen(['sudo', 'systemctl', 'restart', cfg.DAEMON_NAME], cwd=path)
    rcode: int = proc.wait()
    if rcode == 0:
        return True
    else:
        return False


def task_processor_status() -> bool:
    for p in psutil.process_iter():
        cmdline = p.cmdline()
        if len(cmdline) >= 2 and (cmdline[0].find("python") != -1) and (cmdline[1].find(cfg.PROCESSING_LOOP_SCRIPT) != -1):
            return True
    return False


def check_tasks_processor() -> bool:
    try:
        if not task_processor_status():
            return task_processor_start()
        else:
            return True
    except Exception as e:
        print(repr(e))
        return False
