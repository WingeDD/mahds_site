from cerberus import Validator
from api_validation.validation_utils import add_http_method_validation
from db_handle.enums import TaskKind
import application.application_config as cfg


validator_main: Validator = Validator( add_http_method_validation( ["GET"], { } ) )

validator_about: Validator = Validator( add_http_method_validation( ["GET"], { } ) )

validator_result_page: Validator = Validator( add_http_method_validation( ["GET"], { } ) )

validator_result_list: Validator = Validator( add_http_method_validation( ["GET"], { } ) )

validator_result_fetch : Validator = Validator( add_http_method_validation( ["POST"], {
    'id': {'required': True, 'type': 'integer'}
} ) )

validator_task_list : Validator = Validator( add_http_method_validation( ["GET"], { } ) )

validator_task_fetch : Validator = Validator( add_http_method_validation( ["POST"], {
    'id': {'required': True, 'type': 'integer'},
    'needParameters': {'required': False, 'type': 'boolean'},
    'needData': {'required': False, 'type': 'boolean'}
} ) )

validator_task_put : Validator = Validator( add_http_method_validation( ["PUT"], {
    'name': {'required': False, 'type': 'string', 'maxlength': 128},
    'kind': {'required': True, 'type': 'string', 'allowed': [str(TaskKind.ALIGNMENT), str(TaskKind.ASSESSMENT)]},
    'parameters': {
        'required': True,
        'type': 'dict',
        'schema': {
            'sequencesType': {'required': True, 'type': 'string', 'allowed': [cfg.SEQ_TYPE_RDNA, cfg.SEQ_TYPE_PROTEIN, cfg.SEQ_TYPE_PROTEIN_REDUCED]},
            'considerCorrelations': {'required': False, 'type': 'boolean'},
            'hardSignificanceAssessment': {'required': False, 'type': 'boolean'},
            'alignmentType': {'required': False, 'type': 'string', 'allowed': [cfg.AL_TYPE_GLOBAL, cfg.AL_TYPE_LOCAL, cfg.AL_TYPE_BEST_INTERSECTION]},
            'constD': {'required': False, 'type': 'number', 'min': 0.},
            'constE': {'required': False, 'type': 'number', 'min': 0.},
            'multiplierR': {'required': False, 'type': 'number', 'min': 0., 'forbidden': [0.]},
            'constKd': {'required': False, 'type': 'number'},
            'dpMatrixWidth': {'required': False, 'anyof': [
                {'type': 'float', 'min': 0.01},
                {'type': 'integer', 'min': 1}
            ]},
            'randomSeqSetsNumber': {'required': False, 'type': 'integer', 'min': 10, 'max': 300},
            'meanArtificialLength': {'required': False, 'type': 'integer', 'min': 2, 'max': 100000},
            'artificialLengthsRange': {'required': False, 'anyof': [
                {
                    'type': 'dict',
                    'schema': {
                        'lowerBound': {'required': False, 'anyof': [
                            {'type': 'float', 'min': 0., 'max': 2.},
                            {'type': 'integer', 'max': 100000}
                        ]},
                        'upperBound': {'required': False, 'anyof': [
                            {'type': 'float', 'min': 0., 'max': 2.},
                            {'type': 'integer', 'max': 100000}
                        ]},
                        'step': {'required': False, 'type': 'integer', 'min': 1},
                    }
                },
                {
                    'type': 'list',
                    'schema': {'type': 'integer', 'min': 2, 'max': 200000, 'minlength': 1, 'maxlength': 100}
                }
            ]}
        }
    },
    'data': {'required': True}
} ) )

validator_task_delete : Validator = Validator( add_http_method_validation( ["DELETE"], {
    'id': {'required': True, 'type': 'integer'}
} ) )
