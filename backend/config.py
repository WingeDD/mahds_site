from db_handle.enums import UserStatus

DEBUG: bool = True
USING_EXTERNAL_WEBSERVER: bool = False

APPLICATION_ROOT: str = "/mahds" # without / in the end!
SQLALCHEMY_DATABASE_URI: str = 'sqlite:////home/fwd/Desktop/mahds_site/backend/mahds_site.sqlite3'
SQLALCHEMY_TRACK_MODIFICATIONS: bool = False

SECRET_KEY: str = 'abcde'
SECURITY_PASSWORD_SALT: str = '1234'
SESSION_PROTECTION: str = 'strong'
SESSION_COOKIE_SAMESITE: str = 'Strict'

MAIL_SERVER: str = 'mail.fbras.ru'
MAIL_PORT: int = 587
MAIL_USE_TLS: bool = True
MAIL_USE_SSL: bool = False
MAIL_USERNAME: bool = 'no-reply-victoria@biengi.ac.ru'
MAIL_PASSWORD: str = 'GLjFyj4B1FMF'
MAIL_DEFAULT_SENDER: str = ('mahds website', 'no-reply-victoria@biengi.ac.ru')
MAIL_DEBUG: bool = False

FLASK_ADMIN_SWATCH: str = 'flatly'

MAX_CONTENT_LENGTH: int = 1024*1024*1024 + 1000 # 1Gb input data + ~1000b params
DEFAULT_USER_STATUS: UserStatus = UserStatus.USER
