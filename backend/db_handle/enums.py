from strenum import StrEnum


class UserField(StrEnum):
    ID: str = "id"
    EMAIL: str = "email"
    VERIFIED: str = "verified"
    PSW_HASH: str = "psw_hash"
    STATUS: str = "status"
    def __str__(self):
        return str(self.value)

class UserStatus(StrEnum):
    CANDIDATE: str = "candidate"
    USER: str = "user"
    ADMIN: str = "admin"
    def __str__(self):
        return str(self.value)

class TaskField(StrEnum):
    ID: str = "id"
    OWNER: str = "owner"
    NAME: str = "name"
    DATE: str = "date"
    STATUS: str = "status"
    KIND: str = "kind"
    EXECID: str = "execid"
    PARAMETERS: str = "parameters"
    DATA: str = "data"
    def __str__(self):
        return str(self.value)
    
class TaskStatus(StrEnum):
    QUEUED: str = "queued"
    EXECUTING: str = "executing"
    DELETING: str = "deleting"
    DONE: str = "done"
    def __str__(self):
        return str(self.value)

class TaskKind(StrEnum):
    ALIGNMENT: str = "alignment"
    ASSESSMENT: str = "assessment"
    def __str__(self):
        return str(self.value)

class ResultField(StrEnum):
    ID: str = "id"
    OWNER: str = "owner"
    NAME: str = "name"
    DATE: str = "date"
    STATUS: str = "status"
    DATA: str = "data"
    def __str__(self):
        return str(self.value)

class ResultStatus(StrEnum):
    ERROR: str = "error"
    OK: str = "ok"
    def __str__(self):
        return str(self.value)
