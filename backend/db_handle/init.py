from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate


db: SQLAlchemy = SQLAlchemy(
    # engine_options={"execution_options": {"isolation_level": "EXCLUSIVE"}}
)
migrate: Migrate = Migrate()
