import { createRouter, createWebHistory } from 'vue-router'
import NotFoundView from '@/views/NotFoundView'
import LoginView from '@/views/auth/LoginView'
import RegisterView from '@/views/auth/RegisterView'
import VerifyView from '@/views/auth/VerifyView'
import ConfirmView from '@/views/auth/ConfirmView'
import MainView from '@/views/application/MainView'
import ResultView from '@/views/application/ResultView'
import AboutView from '@/views/application/AboutView'

const routes = [
  {
    path: '/auth/login',
    alias: ['/auth', '/auth/main', '/auth/index'],
    name: 'LoginView',
    component: LoginView
  },
  {
    path: '/auth/register',
    name: 'RegisterView',
    component: RegisterView
  },
  {
    path: '/auth/verify',
    name: 'VerifyView',
    component: VerifyView
  },
  {
    path: '/auth/confirm/:token',
    name: 'ConfirmView',
    component: ConfirmView
  },
  {
    path: '/',
    alias: ['/main', '/index'],
    name: 'MainView',
    component: MainView
  },
  {
    path: '/result/page/:id',
    name: 'ResultView',
    component: ResultView
  },
  {
    path: '/about',
    name: 'AboutView',
    component: AboutView
  },
  {
    path: '/:catchAll(.*)',
    name: 'NotFoundView',
    component: NotFoundView
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
